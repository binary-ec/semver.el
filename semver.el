;;; semver.el --- Semver interpreter for Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2024 Guillaume Pasquet

;; Author: Guillaume Pasquet <dev@etenil.net>
;; Keywords: semver helper development
;; X-URL: https://gitlab.com/binary-ec/semver.el
;; URL: https://gitlab.com/binary-ec/semver.el

;; This file is not a part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Semver is a simple library to help you interpret semver version constraints

;;; Usage:

;; Load the file with `(require 'semver)' and then use the functions
;; `semver--expand' or `semver--expand-at-point' to expand a semver
;; spec to a more explicit version constraint.

;;; Contributing:

;; Please feel free to send PRs for any improvements or bug fixes.

;;; Code:

(defun semver--constraint (semver-spec)
  "Extract the constraint indicator from a semver spec.

SEMVER-SPEC is the specification string to extract the constraint from.

Example:
   (semver--constraint \"^1.1.0\") => \"^\"
   (semver--constraint \"1.1.0\") => nil
   (semver--constraint \">=1.1.0\") => \">=\""
  (if (string-match "\\([~^<>=]*\\)" semver-spec)
      (match-string 1 semver-spec)
    nil))

(defun semver--version (semver-spec)
  "Extract the version number from a semver spec as a version triplet.

SEMVER-SPEC is the version string to extract the version triplet from.

Example:
    (semver--version \"^1.1.0\") => (1 1 0)
    (semver--version \"1.1.0\") => (1 1 0)
    (semver--version \">=1.1.0\") => (1 1 0)"
  (if (string-match "\\([~^<>=]*\\)?\\([0-9]+\\)\\.\\([0-9]+\\)\\.\\([0-9]+\\)" semver-spec)
      (list (string-to-number (match-string 2 semver-spec))
            (string-to-number (match-string 3 semver-spec))
            (string-to-number (match-string 4 semver-spec)))))

(defun semver--format-constraint (constraint version)
  "Expresses version constraints in a readable format and retrun as string.
CONSTRAINT is a constraint string like `>=' or `^'.
VERSION is the version triplet as a list like `(major minor patch'."
  (if (not constraint)
        (format "%d.%d.%d" (car version) (cadr version) (caddr version))
      (let ((major (car version))
            (minor (cadr version))
            (patch (caddr version)))
        (cond
         ((string= constraint "^") (format ">=%d.%d.%d <%d.0.0" major minor patch (1+ major)))
         ((string= constraint "~") (format ">=%d.%d.%d <%d.%d.0" major minor patch major (1+ minor)))
         ((string= constraint ">=") (format ">=%d.%d.%d" major minor patch))
         ((string= constraint ">") (format ">%d.%d.%d" major minor patch))
         ((string= constraint "<=") (format "<=%d.%d.%d" major minor patch))
         ((string= constraint "<") (format "<%d.%d.%d" major minor patch))
         ((string= constraint "=") (format "=%d.%d.%d" major minor patch))
         ))))


(defun semver--expand (semver-spec)
  "Translates a semver spec to more explicit version constraints.

SEMVER-SPEC is the version string to extract the version triplet from.

Takes in a semver spec like `^1.1.0' and return the version range in a more
human-readable format like `>=1.1.0 <2.0.0'."
  (interactive "sSemver spec: ")
  (let ((constraint (semver--constraint semver-spec))
        (version (semver--version semver-spec)))
    (message (semver--format-constraint constraint version))))

(defun semver--expand-at-point ()
  "Translates a semver spec at point to more explicit version constraints."
  (interactive)
  (let ((semver-spec (replace-regexp-in-string "^\"\\|\"$" "" (thing-at-point 'string))))
    (message semver-spec)
    (semver--expand semver-spec)))

(provide 'semver)

;;; semver.el ends here
