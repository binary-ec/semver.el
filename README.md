# Semver.el

Semver.el is an Emacs plugin to interpret semver constraints in an
easier to understand form, because no one wants to screw up their
version constraints!

![Semver.el outputting version constraints from a semver string](emacs-semver.png)

## How to install

Download semver.el and place it in your emacs load path, then put the
following in your configuration file:

```elisp
(require 'semver)

(keymap-global-set "C-c C-l" #'semver--expand-at-point)
```

## Usage

This package exposes two interactive functions `semver--expand` and
`semver--expand-at-point`. The first will prompt for a semver string
to expand, whereas the second will attempt to read a semver string
where your cursor is and print the version constraint in the echo
area.